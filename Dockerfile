FROM dkja/crosswin

RUN     cd /tmp \
    && ls /usr \
    && git clone https://github.com/google/googletest \
    && cd googletest \
    && mkdir build \
    && cd build \
    && cmake .. \
        -DCMAKE_SYSTEM_NAME="Generic" \
        -DCMAKE_CXX_COMPILER=x86_64-w64-mingw32-g++-posix \
        -DCMAKE_CC_COMPILER=x86_64-w64-mingw32-gcc-posix \
        -DCMAKE_CXX_FLAGS="-static" \
        -DCMAKE_INSTALL_PREFIX:PATH=/usr/x86_64-w64-mingw32/ \
    && make install \
    && rm -rf /tmp/googletest